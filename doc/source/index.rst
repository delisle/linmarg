linmarg documentation
======================

The linmarg package allows to marginalize a model over linear parameters.
In particular it includes a B-spline model (see [1]_).

Installation
------------

Using conda
~~~~~~~~~~~

The linmarg package can be installed using conda with the following command:

``conda install -c conda-forge linmarg``

Using pip
~~~~~~~~~

It can also be installed using pip with:

``pip install linmarg``

API Reference
-------------

.. autosummary::
   :toctree: _autosummary
   :template: autosummary/custom_module.rst
   :recursive:

   linmarg

References
----------

   .. [1] `Leleu et al., "Removing biases on the density of sub-Neptunes characterised via transit timing variations. Update on the mass-radius relationship of 34 Kepler planets", 2023 <https://ui.adsabs.harvard.edu/abs/2023A&A...669A.117L>`_.
